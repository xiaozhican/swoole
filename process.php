<?php
//for($i=0;$i<10;$i++){
//    sleep(1);
//    echo '正在执行任务';
//}
//$process = new Swoole\Process(function (){});
//$process->start();//启动进程，创建进程

//创建进程
//function doProcess(swoole_process $worker){
//    echo "pid：".$worker->pid."\n";
//    sleep(10);
//}
//$process = new swoole_process("doProcess");
//$pid = $process->start();
//swoole_process::wait();

$workers = [];
$worker_num = 2;
//批量创建进程
for($i=0;$i<$worker_num;$i++){
    $process = new Swoole\Process("doProcess",false,false);
    $process->useQueue();//开启队列
    $pid = $process->start();
    $workers[$pid] = $process;
}
function doProcess(Swoole\Process $process){
    $recv = $process->pop();//默认8192长度
    echo "从主进程获取到数据：$recv \n";
    sleep(5);
    $process->exit(0);
}
//主进程向子进程添加数据
foreach ($workers as $pid=>$process){
    $process->push("Hello，子进程 $pid \n");
}
//等待子进程结束，回收资源
for($i=0;$i<$worker_num;$i++){
    $ret = Swoole\Process::wait();//等待执行完成
    $pid = $ret['pid'];
    unset($workers[$pid]);
    echo "子进程退出 $pid \n";
}