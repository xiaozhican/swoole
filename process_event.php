<?php
$workers = [];//进程池(进程数组)
$worker_num = 3;//创建进程的数量
for($i=0;$i<$worker_num;$i++){
    $process = new Swoole\Process('doProcess');//创建单独新进程
    $pid = $process->start();//启动进程，并获取进程id
    $workers[$pid] = $process;//存入进程数组
}
//创建进程执行函数
function doProcess(Swoole\Process $process){
    $process->write("PID：".$process->pid);//子进程写入信息
    echo "写入信息：{$process->pid}";
}
//添加进程事件，向每一个子进程添加需要执行的动作
foreach ($workers as $process){
    swoole_event_add($process->pipe,function ($pipe) use ($process){
        $data = $process->read();//能否读取数据
        echo "接收到：$data \n";
    });
}